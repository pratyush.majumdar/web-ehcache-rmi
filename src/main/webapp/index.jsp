<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List" %>
<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Get Data</title>
    </head>
    <body>
        <table border="0">
            <tr><td>Id</td><td>Name</td><td>Link</td></tr>
            <c:forEach items="${menu}" var="menuList">
            <tr><td>${menuList.id}</td><td>${menuList.name}</td><td>${menuList.link}</td></tr>
            </c:forEach>
        </table>
        <a href="Statistics">Statistics</a>
    </body>
</html>

package com.pratyush.servlets;

import com.pratyush.mapping.Menu;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class IndexServlet extends HttpServlet {    

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    @SuppressWarnings("unchecked")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        List<Menu> menu = null;
        Session session = null;
        SessionFactory factory = HibernateSessionFactory.getSessionFactory();
        
        try{
            session = factory.openSession();
            Criteria criteria = session.createCriteria(Menu.class);
            criteria.add(Restrictions.eq("status", "ACTIVE"));
            criteria.addOrder(Order.asc("menuOrder"));
            criteria.setCacheable(true);
            menu = criteria.list();
            request.setAttribute("menu", menu);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            session.close();
        }
    }
}

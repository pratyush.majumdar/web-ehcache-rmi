package com.pratyush.servlets;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactory {
    private static SessionFactory sessionFactory = null;

    private HibernateSessionFactory() {
    }

    public static synchronized final SessionFactory getSessionFactory() {
        if(sessionFactory == null)
            sessionFactory = new Configuration().configure().buildSessionFactory();
        return sessionFactory;
    }
}

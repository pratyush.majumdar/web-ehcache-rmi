package com.pratyush.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.SessionFactory;
import org.hibernate.stat.SecondLevelCacheStatistics;

public class GetStatistics extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    @SuppressWarnings("deprecation")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        SessionFactory factory = HibernateSessionFactory.getSessionFactory();
	String regionNames[] = factory.getStatistics().getSecondLevelCacheRegionNames();
        PrintWriter out = response.getWriter();
        String path = request.getServletPath();

        if(path.equalsIgnoreCase("/Statistics")){
            out.print("<table border='0' cellpadding=5>");
            out.println("<tr><th>Region Name</th><th>Hit Count</th><th>Miss Count</th><th>Put Count</th><th>Element Count in Memory</th><th>Element Count on Disk</th><th>Size in Memory</th></tr>");
            for(int i=0;i<regionNames.length;i++) {
                    out.print("<tr>");
                    SecondLevelCacheStatistics statistics = (SecondLevelCacheStatistics) factory.getStatistics().getSecondLevelCacheStatistics(regionNames[i]);
                    out.println("<td style='padding: 15px;'>" + regionNames[i] + "</td>" +
                                    "<td>" + statistics.getHitCount() + "</td>" +
                                    "<td>" + statistics.getMissCount() + "</td>" +
                                    "<td>" + statistics.getPutCount() + "</td>" +
                                    "<td>" + statistics.getElementCountInMemory() + "</td>" +
                                    "<td>" + statistics.getElementCountOnDisk() + "</td>" +
                                    "<td>" + statistics.getSizeInMemory() + "</td>");
                    out.print("</tr>");
            }
            out.print("</table>");
            out.println("<form method=\"POST\" action=\"Invalidate\">");
            out.println("<input type=\"Submit\" value=\"Invalidate\">");
            out.println("</form>");
        }
        else if(path.equalsIgnoreCase("/Invalidate")){
            factory.evictEntity("com.pratyush.mapping.Menu");
            response.sendRedirect("Statistics");
        }
    }
}

# Project Title

Hibernate Second Level Caching with RMI

## Description
While using Second Level Cache in Hibernate, if you are in a distributed environment Cache Invalidation might be a challenge. Nodes participating in the cluster does not know about the cache of other nodes.
Replicated caching using RMI is the solution to this problem.

## To set up replicated caching with RMI you need to configure the CacheManager with:

* A PeerProvider
* A CacheManagerPeerListener

Please visit Official [EHCache page](http://www.ehcache.org/documentation/2.7/replication/rmi-replicated-caching.html) for more information